# -*- coding: utf-8 -*-

# Scrapy settings for GNews_Crawler_Scrapy project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'GNews_Crawler_Scrapy'

SPIDER_MODULES = ['GNews_Crawler_Scrapy.spiders']
NEWSPIDER_MODULE = 'GNews_Crawler_Scrapy.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'GNews_Crawler_Scrapy (+http://www.yourdomain.com)'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Log_file
LOG_FILE ='./logs/gnews.log'
LOG_LEVEL ='INFO'

# Feed Export
FEED_URI ='./feeds/gnews_%(time)s.json'
FEED_FORMAT ='json'

# Google Authentication
G_COOKIES = {
    '1P_JAR': '2019-7-8-11',
    'NID': '187=KTcZuid0jdDU1mb3VFRx5uCeb-S4KytE6flV6LOMOahFLHGrQwo1IT8TfNb3FTfDIqjnPrrX1EWhWqPcfkwjp9w4Olu3j0WripwOdHR4mpajlwoMrZ00T9qYxriSsh8UbkRnwBpxiqNZZTKsXX4HAOq-0v8hFI5muPWa9IMgWvIAD8Znx8I4UZHhy7mf9O0jbllAj69dL-h0nowy3V82UDfwD7v6Vx1k-yY1vA',
    'CONSENT': 'YES+ES.en-GB+20160214-16-0',
    'OGP': '-5061451:',
    'ANID': 'AHWqTUkv0aSeE9UPQdeCi_T6h4qCZLDTOsCrHZLL4JXBIXVegZdR9loxyieW_Er-',
    '_ga': 'GA1.3.2037884602.1561730836',
    'OTZ': '4990447_48_52_123900_48_436380',
    'SID': 'lAck9ZGT2u1fjo4JE2Zlz3xHK1X-daUuq5hRDpYAbW6Oqln-WEjTNeDwjy8aAF3jVYFOfg.',
    'HSID': 'AtqgEtq9ElKqnxfFO',
    'SSID': 'AKeAK6hgJ-VvWE8jM',
    'APISID': 'p3IJdakngg90TTOB/AlogHDjtr2Bz7UY_a',
    'SAPISID': 'DzI-Uo2FLnftUMVL/AbzupHrn84UPdiVbA',
    'SIDCC': 'AN0-TYsxpiBlWK5OCQ_VJ4F3xIGDVoKDNwN3SB4iKFXNlkj_dgrjazQHZ1kd3ruj1Uyey5-2dxYX',
    'SEARCH_SAMESITE': 'CgQIoI0B',
    '_gid': 'GA1.3.2016447151.1562574625',
    '_gat_UA1155451512': '1',
}

G_HEADERS = {
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:67.0) Gecko/20100101 Firefox/67.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '0',
    'Cache-Control': 'max-age=0',
    'TE': 'Trailers',
}

G_EMAIL = 'something'
G_PASSOWRD = 'password'


# @TODO: append query string to initial request
G_QUERY_STRING = (
    ('pageId', 'none'),
    ('hl', 'en-US'),
    ('gl', 'US'),
    ('ceid', 'US:en'),
)

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
EXTENSIONS = {
    'scrapy.extensions.closespider.CloseSpider': 500,
}
# Setings for scrapy.extensions.closespider.CloseSpide
CLOSESPIDER_ERRORCOUNT = 1


# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    #'GNews_Crawler_Scrapy.pipelines.RemoveDuplication': 200,
    'scrapy.pipelines.images.ImagesPipeline': 300,
    'GNews_Crawler_Scrapy.pipelines.MongoDbPipeline': 400,
    
}
# Setings for GNews_Crawler_Scrapy.pipelines.MongoDbPipeline
MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_DB = 'gnews'
# Setings for scrapy.pipelines.images.ImagesPipeline
IMAGES_STORE = './images'
    
    
# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'GNews_Crawler_Scrapy.middlewares.GnewsCrawlerScrapySpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'GNews_Crawler_Scrapy.middlewares.GnewsCrawlerScrapyDownloaderMiddleware': 543,
#}


# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
