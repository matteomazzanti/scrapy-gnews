# Scrapy-GNews

A web-spider implemented with Scrapy, to crawl GoogleNews selected topics 
and personal searches.

A list of new artiles will be inserted in mongo-db.  
You can visualize and manage it with the Scrapy-GNews-UI package.

## Getting Started

### Prerequisites

The software can run on Windows, MacOS and Linux, as it is wirtten in Python language. 
However, the installing instruction dscribe the procedure for Linux OS.

- Python >= 3.5.2
- pip >= 19.1.1
- MongoDB >= 2.6.10


### Installing (Linux)

* (Optional) Create project folder 'GNEWS' and go into it:  
`$ mkdir GNEWS & cd GNEWS`
* Create a virtual python-environment and go into it:   
`$ python3 -m venv GNews_Crawler & cd GNews_Crawler`
* Activate the python virtual environment:
`$ source ./bin/activate`  
* Clone the GIT repository:
`$ git clone https://gitlab.com/taklia/scrapy-gnews.git`
* Copy GNews_Crawler/scrapy-gnews/extras/geckodriver into GNews_Crawler/bin/geckodriver 
`$ cp ./scrapy-gnews/extras/geckodriver ./bin/` 
* Update pip to the last version:
`pip install --upgrade pip`
* Install the python dependencies (the packages will be installed 
for the virtual environment only):
`$ pip install -r scrapy-gnews/requirements.txt`
* (Optional) Configure the mongo-db location
  * Open the file `scrapy-news/GNews_Crawler_Scrapy/settings.py`
  * Change the parameter `MONGODB_URI`
* The spider need some cnfiguration before it can crawl GNews: in order to access 
your personal GNews pages, you need to set a cookie.
  * Login into Google account and browse https://news.google.com.
  * press F12, the developer tool window will appear.
  * Switch to the developer tool Network tab.
  * Click `reload`, a list of request will be shown.
  * Right click on the request `news.google.com` and select `copy as cURL`
  * Browse https://curl.trillworks.com/ and paste the cURL commands in the cURL textbox.
  * Copy the cookie object.
  * Open the file `scrapy-news/GNews_Crawler_Scrapy/settings.py`
  * Replace the content of `G_COOKIES` parameter with the copied cookie object  


The spider is now ready to crawl Google News.  
From the folder `scrapy-gnews` run the scrapy command:
`scrapy crawl gnews`  

The spider will start to crawl.  
Logs available in log/gnews.log


## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/taklia/scrapy-gnews/-/tags). 


## Authors

* **Matteo Mazzanti** - *Initial work* - [Taklia.com](https://gitlab.com/taklia)

See also the list of [contributors](https://gitlab.com/taklia/scrapy-gnews/-/graphs/develop) who participated in this project.


## License

This project is licensed under the GNUv3 License.


## Inspiring

For those who appreciate the power of GNews but dislike the GNews's UI 
(it presents the news in random order, it doesn't mark the new ones, etc..), 
I have created this spider that collect the news from GNews's personal topics and searches 
and insert them in MongoDB.  
The news are then presented in a user-friendly way, an easily managed by using the 
complementary package `Scrapy-GNews-UI`.