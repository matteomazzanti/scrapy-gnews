# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import MapCompose, TakeFirst

def entities_to_chars(value):
    # replace dash 
    value = value.replace(u"\u2013", '')
    # replace left-single-quotation-mark
    value = value.replace(u"\u2018", "'")
    # replace right-single-quotation-mark
    value = value.replace(u"\u2019", "'")
    # replace left-double-quotation-mark
    value = value.replace(u"\u201c", "'")
    # replace right-double-quotation-mark
    value = value.replace(u"\u201d", "'")
    # replace hrizontal hellipsis
    value = value.replace(u"\u2026", '...')
    # replace latin small letter 'e' with acute
    value = value.replace(u"\u00e9", 'é')
    # replace en dash
    value = value.replace(u"\u2013", '-')
    # replace em dash
    value = value.replace(u"\u2014", '-')
    # replace zero width space
    value = value.replace(u"\u200b", '')
    return value


class ArticleItem(scrapy.Item):
    topic = scrapy.Field(
        output_processor= TakeFirst()
    )
    topic_type = scrapy.Field(
        output_processor= TakeFirst()
    )
    title = scrapy.Field(
        input_processor= MapCompose(entities_to_chars),
        output_processor= TakeFirst()
    )
    source = scrapy.Field(
        output_processor= TakeFirst()
    )
    publication_datetime = scrapy.Field(
        output_processor= TakeFirst()
    )
    gurl = scrapy.Field(
        output_processor= TakeFirst()
    )
    url = scrapy.Field(
        output_processor= TakeFirst()
    )
    image_urls = scrapy.Field()
    images = scrapy.Field()
    reviewed = scrapy.Field(
        output_processor= TakeFirst()
    )
