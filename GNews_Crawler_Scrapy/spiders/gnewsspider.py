# gnewsspider.py

import logging
from pprint import pprint
import time
import urllib
import urllib.parse as urlparse

import scrapy
from scrapy.loader import ItemLoader
from GNews_Crawler_Scrapy.items import ArticleItem

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.options import Log
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


# Debug code examples:
#
# scrapy.utils.response.open_in_browser(response)
#
# self.crawler.stats.set_value('key', value)
#
# from scrapy.shell import inspect_response
# inspect_response(response, self)


class GNewsSpider(scrapy.Spider):
    """Spider for crawling Google News.
    
    Entry point for GNews crawling operations.
    """
    
    name = 'gnews'


    def start_requests(self):
        print("GNews-Spider starts...")
        
        # Selenium browser init and load news.google page
        options = Options()
        options.headless = True
        log = Log()
        log.level = "CRITICAL"
        options.add_argument(log.level)
        self.driver = webdriver.Firefox(options=options, service_log_path='logs/geckodriver.log')
        self.driver.get('https://accounts.google.com/signin/v2/identifier?hl=en&continue=https://news.google.com/')   
        
        # login into Google with Selenium
        email_str = self.settings.get('G_EMAIL')
        password_str = self.settings.get('G_PASSOWRD')
        self.driver.find_element_by_id("identifierId").send_keys(email_str)
        self.driver.find_element_by_id("identifierNext").click()
        time.sleep(2)
        password = WebDriverWait(self.driver, 3).until(
            EC.presence_of_element_located((By.XPATH, "//input[@type='password']" )))
        password.send_keys(password_str)
        element = self.driver.find_element_by_id('passwordNext')
        self.driver.execute_script("arguments[0].click();", element) 

        # Browse pages with Scrapy spider
        self.cookies = self.settings.get('G_COOKIES')
        self.headers = self.settings.get('G_HEADERS')
        return [scrapy.Request(url='https://news.google.com/', 
                              headers=self.headers, 
                              cookies=self.cookies,
                              meta={'cookiejar': 'https://news.google.com/'},
                              callback=self.parse_index)]
    
    
    def parse_index(self, response):
        if 'Sign in' == response.xpath("//header/div[2]/div[3]/div/a/text()").get():
            print('GNews-Spider has NOT logged in Google.')
            logging.error('GNews-Spider has NOT logged in Google.')
            return
        print('GNews-Spider is on the index page.')
        return [scrapy.Request(url='https://news.google.com/my/library', 
                                headers=self.headers, 
                                meta={'cookiejar': response.meta['cookiejar']},
                                callback=self.parse_library),
                scrapy.Request(url='https://news.google.com/my/searches', 
                                headers=self.headers, 
                                meta={'cookiejar': response.meta['cookiejar']},
                                callback=self.parse_searches)
               ]

        
    def parse_library(self, response):
        print("GNews-Spider is parsing the library topics list.")
        # Extract links for library's topics and follow them
        for href in response.xpath('//c-wiz/div/div/main/div/div/a/@href').getall():
            yield response.follow(href, 
                                  callback=self.parse_libray_topic,
                                  headers=self.headers, 
                                  meta={'cookiejar': response.meta['cookiejar']})
    
        
    def parse_libray_topic(self, response):  
        topic_type = 'library'
        topic_name = response.xpath("//c-wiz/div/div[2]/c-wiz/div/div[1]/div[1]/div/div[2]/h2/text()").get()
        if topic_name is not None:
            print("GNews spider is parsing the " + topic_type + " page: " + topic_name)
            # @TODO Parse special topics like 'Artificial Intelligence' in specialized parse-functions
            # Use Selenium to load pages, infinite scroll, get the page
            # content and pass it to Scrapy 
            self.driver.get(response.url)
            for _i in range(1,5):
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                time.sleep(2)
            response = response.replace(body=self.driver.page_source)
            # Collect the articles
            for article in response.xpath('//c-wiz/div/div[2]/div/main/c-wiz/div/div/main/div[1]/div'):
                itemLoader = ItemLoader(item=ArticleItem(), selector=article, response=response)
                itemLoader.add_value('topic', topic_name)
                itemLoader.add_value('topic_type', topic_type)
                itemLoader.add_xpath('title', ".//div/article/h3/a/text()")
                itemLoader.add_value('gurl', response.urljoin( article.xpath(".//div/article/h3/a/@href").get() ))
                itemLoader.add_xpath('source', ".//div/article/div[2]/div/a/text()")
                itemLoader.add_xpath('publication_datetime', ".//div/article/div[2]/div/time/@datetime")
                image_urls = article.xpath(".//a/figure/img/@src").get()
                if image_urls is not None:
                    itemLoader.add_value('image_urls', image_urls)
                itemLoader.add_value('reviewed', False)
                itemLoader.load_item()
                yield response.follow(itemLoader.item['gurl'], self.parse_article, meta={'itemLoader':itemLoader})


    def parse_article(self, response):
        itemLoader = response.meta['itemLoader']
        url = response.xpath('//c-wiz/div/div[2]/c-wiz/div[2]/noscript/a/@href').get()  
        if url is not None:      
            itemLoader.add_value('url', url)
            yield itemLoader.load_item()
        
        
        
        
    def parse_searches(self, response):
        print("GNews-Spider is parsing the custom-searches topics list.")
        # Extract links for searches's topics and follow them
        for href in response.xpath('//c-wiz/div/div/main/div/div/a/@href').getall():
            urlparsed = urlparse.urlparse(href)
            topic_name = urllib.parse.parse_qs(urlparsed.query)['q'][0]
            yield response.follow(href, 
                                  callback=self.parse_searches_topic,
                                  headers=self.headers, 
                                  meta={'cookiejar': response.meta['cookiejar'],
                                        'topic_name':topic_name})
    
        
    def parse_searches_topic(self, response):
        topic_type = 'searches'
        topic_name = response.meta['topic_name']
        if topic_name is not None:
            print("GNews spider is parsing the " + topic_type + " page: " + topic_name)
            # Use Selenium to load pages, infinite scroll, get the page
            # content and pass it to Scrapy 
            self.driver.get(response.url)
            for _i in range(1,5):
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                time.sleep(2)
            response = response.replace(body=self.driver.page_source)
            # Collect the articles
            for article in response.xpath('//c-wiz/div/div[2]/div/div/main/c-wiz/div[1]/div'):
                itemLoader = ItemLoader(item=ArticleItem(), selector=article, response=response)
                itemLoader.add_value('topic', topic_name)
                itemLoader.add_value('topic_type', topic_type)
                itemLoader.add_xpath('title', ".//div/article/h3/a/text()")
                itemLoader.add_value('gurl', response.urljoin( article.xpath(".//div/article/h3/a/@href").get() ))
                itemLoader.add_xpath('source', ".//div/article/div[2]/div/a/text()")
                itemLoader.add_xpath('publication_datetime', ".//div/article/div[2]/div/time/@datetime")
                image_urls = article.xpath(".//a/figure/img/@src").get()
                if image_urls is not None:
                    itemLoader.add_value('image_urls', image_urls)
                itemLoader.add_value('reviewed', False)
                itemLoader.load_item()
                yield response.follow(itemLoader.item['gurl'], self.parse_article, meta={'itemLoader':itemLoader})
